package traba3.server;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        ServerSocket serverSocket;
        Socket socketClient = null;    
        int port = 5540;
        boolean continuar = true;

        //etapa de binding IP e Port
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Servidor disponivel na porta: " + port);
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
            return;
        }

        //etapa de aceite de conexao
        while(continuar){
        try {
            System.out.println("Aguardando o Client.");
            socketClient = serverSocket.accept();
            System.out.println("Conectado com " + socketClient.getInetAddress().getHostAddress());    

            try {
                Thread parImpar = new ThreadParImpar(socketClient);
                parImpar.start();
            } catch (Exception e) {
                // TODO: handle exception
            }


        } catch (Exception e) {
            System.out.println("Erro no aceite do client");
        }
    }


        //etapa de encerramento
        try {
            System.out.println("Encerrando o servidor");
            serverSocket.close();
            socketClient.close();
        } catch (Exception e) {
            System.out.println("Erro no encerramento.");
        }
    }
}