package traba3.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import traba3.util.MsqReq;
import traba3.util.MsqResp;
import traba3.util.Status;

public class ThreadParImpar extends Thread {
    private Socket client;
    ObjectInputStream in;
    ObjectOutputStream out;
    MsqResp response;

    

    public ThreadParImpar(Socket client) {
        this.client = client;
        
    }



    @Override
    public void run() {
        try {
            in = new ObjectInputStream(client.getInputStream());
            out = new ObjectOutputStream(client.getOutputStream());
            MsqReq request = (MsqReq) in.readObject();

            System.out.println("Recebido: " + request.getParImpar() + " " + request.getValue1());

            String parImpar = request.getParImpar();
            int value1 = request.getValue1();
            int value2;
            String resp;

            switch (parImpar) {
                case "par":
                    value2 = value1 % 2;
                    if (value2 == 0) {
                        resp = "Vencedor!";
                        response = new MsqResp(Status.VENCEDOR, resp);
                    } else {
                        resp = "Perdeu!";
                        response = new MsqResp(Status.PERDEDOR, resp);
                    }

                    break;

                    case "impar":
                    value2 = value1 % 2;
                    if (value2 == 1) {
                        resp = "Vencedor!";
                        response = new MsqResp(Status.VENCEDOR, resp);
                    } else {
                        resp = "Perdeu!";
                        response = new MsqResp(Status.PERDEDOR, resp);
                    }

                    break;

                default:
                    response = new MsqResp(Status.ERRO_NUMERO_INVALIDO, "Erro");
                    break;
            }

            out.writeObject(response);
        } catch (Exception e) {
            System.out.println("Erro na Thread: " + e.getMessage());
        }
        
    }
}
