package traba3.client;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

import traba3.util.MsqReq;
import traba3.util.MsqResp;
import traba3.util.Status;

public class Client {
    

    public static void main(String[] args) {
        int port = 5540;
        String ip = "127.0.0.1";
        Socket socket = null;
        ObjectOutputStream out;
        ObjectInputStream in;
        Scanner entrada = new Scanner(System.in);
        int value1;
        String parImpar;
        String username;
        String modo = " ";

  
        try {
            System.out.println("Informe um username.");
            username = entrada.nextLine();
            System.out.println("Escreva se quer jogar multiplayer ou singleplayer.");
            modo = entrada.nextLine();
        } catch (Exception e) {
            System.out.println("Não consegimos adicionar seu username.");
        }
    

    
        if (modo == "singleplayer") {
            System.out.println("Escolha impar ou par.");
            parImpar = entrada.nextLine();
            System.out.println("Escolha um valor entre 0 a 5.");
            value1 =  Integer.parseInt(entrada.nextLine());
            Random random = new Random();
            int numeroComputador = random.nextInt(6);
            System.out.println("Computador escolheu: " + numeroComputador);
            int total = value1 + numeroComputador;
            
            switch (parImpar) {
                case "impar":
                    int resposta = total % 2;
                    if (resposta == 1) {
                        System.out.println("Parabens você ganhou!");
                    } else {
                        System.out.println("você perdeu!");
                    }
                    break;
                    
                case "par":
                 resposta = total % 2;
                if (resposta == 0) {
                    System.out.println("Parabens você ganhou!");
                } else {
                    System.out.println("você perdeu!");
                }
                break;
                default:
                System.out.println("Erro na variavel.");
                    break;
            }

        } else {
        // Etapa de conexao
            try {
                socket = new Socket(ip,port);
                System.out.println("Conectado com o Servidor");
            } catch (Exception e) {
                System.out.println("Erro: " + e.getMessage());
                return;
            }
            try {


                out = new ObjectOutputStream(socket.getOutputStream());
                in = new ObjectInputStream(socket.getInputStream());
    
                System.out.println("Digite se é par ou impar: ");
                parImpar = entrada.nextLine(); 
                System.out.println("Digite um valor entre 0 a 5: ");
                value1 = Integer.parseInt(entrada.nextLine());
    
    
                MsqReq request = new MsqReq(parImpar, value1);
    
                out.writeObject(request);
    
                MsqResp response = (MsqResp) in.readObject();
    
                if(response.getStatus() == Status.VENCEDOR){
                    System.out.println("Resposta: " + response.getResp());
                } else {
                    if(response.getStatus() == Status.PERDEDOR){
                        System.out.println("Perdeu");
                    }else{
                        System.out.println("Erro");
                    }
                }
    
            } catch (Exception e) {
                System.out.println("Erro na comunicação: " + e.getMessage());
            }
        }
       
        
        try {
            System.out.println("Encerrando conexao");
            entrada.close();
            if (socket.isConnected()) {
                socket.close();    
            }
        } catch (Exception e) {
            System.out.println("Erro no encerramento. " + e.getMessage());
        }
    }
}
