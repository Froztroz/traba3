package traba3.util;

import java.io.Serializable;

public class MsqResp implements Serializable{
    private Status status;
    private String resp;

    public MsqResp(Status status, String resp) {
        this.status = status;
        this.resp = resp;
    }

    public Status getStatus() {
        return status;
    }

    public String getResp() {
        return resp;
    }
    

    
    
}
